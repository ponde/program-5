/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author EP
 */
public class GameState {
   
   private int highScore = 0;
   private int timer = 20;
   private int lives = 3;
   private int score = 0;
   
   public int getTime()
   {
      return timer;
   }
   
   public int getScore()
   {
      return score;
   }
   
   public int getLives()
   {
      return lives;
   }
   
   public void increaseScore(int i)
   {
      score += i;
   }
   
   public void increaseTime(int i)
   {
      timer += i;
   }
   
   public void decreaseTime()
   {
      if (timer > 0)
         timer -= 1;
      //else
         //game over
   }
   
   public void addLife()
   {
      lives += 1;
   }
   
   public void subtractLife()
   {
      if (lives > 0)
         lives -= 1;
      else
         lives = 0; //game over
   }
}
