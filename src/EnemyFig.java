
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.util.Random;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author EP
 */
public class EnemyFig extends PFigure 
{

   public EnemyFig(int startX, int startY, int _width, int _height, int pr, JPanel p) { //should be super
      super(startX, startY, _width, _height, pr, p);
   }
   
   @Override
   public void move()
   {
      Random random = new Random();
      int randomX = random.nextInt(30) - 15;
      int randomY = random.nextInt(30) - 15;
      
      if(x + randomX < 0 || x + randomX > panel.getWidth())
         randomX = 0;
      if(y + randomY < 0 || y + randomY > panel.getHeight())
         randomY = 0;
      
      super.move(randomX, randomY);
   }

   @Override
   public void draw() 
   {
      Graphics g = panel.getGraphics();
      
      g.setColor(Color.green);
      g.fillRect(x, y, width, height);
   }
   
}
