/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steven
 */
public class PFigureList 
{
   
   private final int GROW_BY = 3;
   
   private PFigure[] elements = new PFigure[0];
   private int num = 0;
   
   public boolean add(PFigure p)
   {
      if(num == elements.length)
         grow();
      
      elements[num++] = p;
      return true;
   }
   
   public boolean remove(PFigure p)
   {
      int index = find(p);
      if(index == -1)
         return false;
      
      for(int i = index; i < num; i++)
      {
         if(i + 1 == elements.length)
            elements[i] = null;
         
         else
            elements[i] = elements[i + 1];
      }
      
      num--;
      
      return true;
   }
   
   public PFigure getItemAt(int index)
   {
      return elements[index];
   }
   
   public void updateFigures()
   {
      for(int i = 0; i < num; i++)
      {
         elements[i].hide();
         elements[i].move();
         elements[i].draw();
      }
   }
   
   private void grow()
   {
      PFigure holder[] = new PFigure[elements.length + GROW_BY];
      for(int i = 0; i < num; i++)
         holder[i] = elements[i];
      
      elements = holder;
   }
   
   private int find(PFigure p)
   {
      for(int i = 0; i < num; i++)
         if(p.equals(elements[i]))
            return i;
       
      return -1;
   }
   
}
