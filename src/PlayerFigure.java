
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steven
 */
public class PlayerFigure extends PFigure
{
   
   public PlayerFigure(JPanel p)
   {
      super(0, 0, 20, 20, 0, p);
      
      x = panel.getWidth() / 2;
      y = panel.getHeight()/ 2;
   }

   @Override
   public void move(int deltaX, int deltaY)
   { 
      if(x + deltaX < 0 || x + deltaX > panel.getWidth() - width)
         deltaX = 0;
      
      if(y + deltaY < 0 || y + deltaY > panel.getHeight() - height)
         deltaY = 0;
      
      super.move(deltaX, deltaY);
   }
   
   public void die()
   {
      //TODO: Implement die behavior
   }
   
   private void respawn()
   {
      //TODO: if not out of lives, respawn from die method
   }
   
   @Override
   public void draw() 
   {
      Graphics g = panel.getGraphics();
      g.setColor(Color.red);
      g.fillRect(x, y, width, height);
   }
   
}
